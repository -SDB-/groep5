import pandas as pd
import DateTime as dt
import numpy as np
import sqlite3

#laat dataframe goed zien
#pd.set_option('display.max_columns', None)
#pd.set_option('display.width', 320)
#pd.set_option('display.max_colwidth', 320)
#pad waar het database bestand zich bevind
path = "/Users/Gebruiker5/Documents/GROEP5/"
#connectie wordt gemaakt naar de database
con = sqlite3.connect(path + "Database_groep5.sqlite")
# 3 connecties voor de drie tabellen
cursorObj = con.cursor()

#select statements worden toegepast op de database
cursorObj.execute('SELECT * FROM Ethereum_koers')

#alle data wordt opgevraagd
eth = cursorObj.fetchall()

#dataframes worden aangemaakt, kolommen krijgen correcte namen, datum krijgt juiste type
eth = pd.DataFrame(eth)
eth = eth.rename(columns={0 : 'ID',1 : 'Datum/tijd', 2:'Actueel', 3 : 'Hoogst', 4 : 'Laagst', 5 : 'Sluit', 6 : 'Open'})
eth['Datum/tijd'] = pd.to_datetime(eth['Datum/tijd'])
eth['ID'] = 0

#df = pd.read_csv(r'C:\Users\cecie\Desktop\Ethereum_koers.csv') test path
#splitst colomn en haalt duplicaten van de datums weg
eth['Datum/tijd'] = pd.to_datetime(eth['Datum/tijd'])
eth['date'] = eth['Datum/tijd'].dt.date
eth['time'] = eth['Datum/tijd'].dt.time
del eth['Datum/tijd'], eth['time']
eth = eth.drop_duplicates('date', keep='last')

#df = df.set_index('date')
#berekend de voortschrijdende gemiddeldes met een reeks van 3 en een reeks van 7
for i in range(0, eth.shape[0] - 2):
       eth.loc[eth.index[i + 2], 'SMA_3'] = np.round(
          (
                  (eth.iloc[i, 5] +
                   eth.iloc[i + 1, 5] +
                   eth.iloc[i + 2, 5]) / 3),1)
for i in range(0, eth.shape[0] - 6):
    eth.loc[eth.index[i + 6], 'SMA_7'] = np.round(
        (
                (eth.iloc[i , 5] +
                 eth.iloc[i + 1, 5] +
                 eth.iloc[i + 2, 5] +
                 eth.iloc[i + 3, 5] +
                 eth.iloc[i + 4, 5] +
                 eth.iloc[i + 5, 5] +
                 eth.iloc[i + 6, 5]) / 7),1)
#maakt advies tabel aan, en algoritme functie
def advies(row):
    if row['SMA_3'] > row['SMA_7']:
        return 'Inkopen'
    else: return 'Verkopen'
eth['advies'] = eth.apply(advies, axis=1)
#zet in dataframe
df2 = pd.DataFrame()

df2['Date'] = eth['date']

df2['SMA_3'] = eth['SMA_3']

df2['SMA_7'] = eth['SMA_7']

df2['Advies'] = eth['advies']


#connectie wordt gemaakt naar de database
cnx = sqlite3.connect(path + "Database_groep5.sqlite")

#bestand/dataframe wordt toegevoegd aan de meegegeven database
df2.tail(1).to_sql(name="Advies", con=cnx, index=False, if_exists= 'append')

#aanpassingen worden gecommit
cnx.commit()

#connectie wordt gesloten
cnx.close()
print(df2)
print('Klaar')
