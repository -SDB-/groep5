import yfinance as yf
import requests
import datetime as dt
import pandas as pd
from pandas.io.json import json_normalize
import sqlite3


#laat dataframe goed zien
pd.set_option('display.max_columns', None)
pd.set_option('display.width', 320)
pd.set_option('display.max_colwidth', 320)

#haalt candlestick data op
data = yf.download("ETH-EUR", start=(dt.datetime.now() - dt.timedelta(hours=1)), end=dt.datetime.now(), group_by = 'ticker')

#verkrijgt aan de hand van een api actuele prijs
link = str("https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=EUR" )
Data = requests.get(link)
results = Data.json()
test = json_normalize(results)

#huidige datum/tijd in variable
datum = dt.datetime.now().strftime('%Y-%m-%d %H:%M')

#actuele prijs in variable
prijs = test.at[0, 'EUR']

#herstelt dataframe door index te resetten en verwijderd 3 kolommen
data = data.reset_index()
data = data.drop(columns=('Date'))
data = data.drop(columns=('Adj Close'))
data = data.drop(columns=('Volume'))

#alle nodige data wordt in bijbehorende variable gestopt
Open = data.at[0, 'Open']
Close = data.at[0, 'Close']
High = data.at[0, 'High']
Low = data.at[0, 'Low']
Price = test.at[0, 'EUR']

#dictionary met alle data wordt gemaakt
ETH = {'Datum/tijd' : [datum],
        'Actueel' : [round(Price, 2)],
        'Hoogst' : [round(High, 2)],
       'Laagst' : [round(Low, 2)],
       'Sluit' : [round(Close, 2)],
       'Open' : [round(Open, 2)]}

#pad waar het database bestand zich bevind
path = "/Users/Gebruiker5/Documents/GROEP5/"

#ETH wordt omgezet naar een dataframe
df = pd.DataFrame(ETH)

#connectie wordt gemaakt naar de database
cnx = sqlite3.connect(path + "Database_groep5.sqlite")

#bestand/dataframe wordt toegevoegd aan de meegegeven database
df.to_sql(name="Ethereum_koers", con=cnx, index=False, if_exists= 'append')

#aanpassingen worden gecommit
cnx.commit()

#connectie wordt gesloten
cnx.close()

print('klaar')

