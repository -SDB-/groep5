import pandas as pd
import sqlite3

#laat dataframe goed zien
#pd.set_option('display.max_columns', None)
#pd.set_option('display.width', 320)
#pd.set_option('display.max_colwidth', 320)

#pad waar het database bestand zich bevind
path = "/Users/Gebruiker5/Documents/GROEP5/"

#connectie wordt gemaakt naar de database
con = sqlite3.connect(path + "Database_groep5.sqlite")

# 3 connecties voor de drie tabellen
cursorObj = con.cursor()
cursorObj1 = con.cursor()
cursorObj2 = con.cursor()

#select statements worden toegepast op de database
cursorObj.execute('SELECT * FROM Ethereum_koers')
cursorObj1.execute('SELECT * FROM AEX_koers')
cursorObj2.execute('SELECT * FROM Advies')

#alle data wordt opgevraagd
eth = cursorObj.fetchall()
aex = cursorObj1.fetchall()
Advies = cursorObj2.fetchall()

#dataframes worden aangemaakt, kolommen krijgen correcte namen, datum krijgt juiste type
eth = pd.DataFrame(eth)
eth = eth.rename(columns={0 : 'ID',1 : 'Datum/tijd', 2:'Actueel', 3 : 'Hoogst', 4 : 'Laagst', 5 : 'Sluit', 6 : 'Open'})
eth['Datum/tijd'] = pd.to_datetime(eth['Datum/tijd'])
eth['ID'] = 0

aex = pd.DataFrame(aex)
aex = aex.rename(columns={0 : 'ID',1 : 'Datum/tijd', 2:'Actueel', 3 : 'Hoogst', 4 : 'Laagst', 5 : 'Sluit', 6 : 'Open'})
aex['Datum/tijd'] = pd.to_datetime(aex['Datum/tijd'])
aex['ID'] = 0

Advies = pd.DataFrame(Advies)
Advies = Advies.rename(columns={0 : 'Date',1 : 'Advies'})
Advies['Date'] = pd.to_datetime(Advies['Date'])

# pakt alleen de laaste rij van de dataframes
eth = eth.tail(1)
aex = aex.tail(1)
Advies = Advies.tail(1)
                       
print(eth)
print(aex)
print(Advies)

#pad waar het database bestand zich bevind
path = "/Users/Gebruiker5/Documents/GROEP5/"

#connectie wordt gemaakt naar de database
cnx = sqlite3.connect(path + "Staging_area_groep5.sqlite")

#bestand/dataframe wordt toegevoegd aan de meegegeven database
eth.to_sql(name="Ethereum_koers", con=cnx, index=False, if_exists= 'replace')
aex.to_sql(name="AEX_koers", con=cnx, index=False, if_exists= 'replace')
Advies.to_sql(name="Advies", con=cnx, index=False, if_exists= 'replace')


#aanpassingen worden gecommit
cnx.commit()

#connectie wordt gesloten
cnx.close()

print('klaar')
