from bs4 import BeautifulSoup
import pandas as pd
from requests import get
import datetime as dt
import sqlite3

#stopt de webpagina in een variabele
url1 = 'https://www.beursgorilla.nl/koersen/aex.aspx'
url2 = 'https://www.debeurs.nl/Index-Koers/12272/AEX.aspx'
url3 = 'https://www.beleggen.nl/Index-Koers/12272/AEX.aspx'

#open als het ware de webpagina
response1 = get(url1)
response2 = get(url2)
response3 = get(url3)

#zorgt ervoor dat de webpagina gelezen kan worden
html_soup1 = BeautifulSoup(response1.text, 'html.parser')
html_soup2 = BeautifulSoup(response2.text, 'html.parser')
html_soup3 = BeautifulSoup(response3.text, 'html.parser')

#type wordt bepaald
type(html_soup1)
type(html_soup2)
type(html_soup3)

#zorgt ervoor dat de juiste waardes worden gevonden in de html
Actueel1 = html_soup1.find("span", id='12272LastPrice')
Hoogst1 = html_soup1.find("span", id='12272HighPrice')
Laagst1 = html_soup1.find("span", id='12272LowPrice')
Sluit1 = html_soup1.find("span", id='12272ClosePrice')
Open1 = html_soup1.find("span", id='12272ClosePrice')

Actueel2 = html_soup2.find("span", id='ctl00_ctl00_Content_ContentLeft_IssueSnapshot_IssueDetails_lblLastPrice')
Hoogst2 = html_soup2.find("span", id='ctl00_ctl00_Content_ContentLeft_IssueSnapshot_IssueDetails_lblDayHigh')
Laagst2 = html_soup2.find("span", id='ctl00_ctl00_Content_ContentLeft_IssueSnapshot_IssueDetails_lblDayLow')
Sluit2 = html_soup2.find("span", id='ctl00_ctl00_Content_ContentLeft_IssueSnapshot_IssueDetails_lblClosePrice')
Open2 = html_soup2.find("span", id='ctl00_ctl00_Content_ContentLeft_IssueSnapshot_IssueDetails_lblOpenPrice')

Actueel3 = html_soup3.find("span", id='12272LastPrice')
Hoogst3 = html_soup3.find("span", id='12272HighPrice')
Laagst3 = html_soup3.find("span", id='12272LowPrice')
Sluit3 = html_soup3.find("td", attrs={'class' : 'ut--right'})
Open3 = html_soup3.find("td", attrs={'class' : 'ut--right ut--bold'})

#pakt de nodige waarde uit de gescrapte tekst
Actueel1 = Actueel1.text
Hoogst1 = Hoogst1.text
Laagst1 = Laagst1.text
Sluit1 = Sluit1.text
Open1 = Open1.text

Actueel2 = Actueel2.text
Hoogst2 = Hoogst2.text
Laagst2 = Laagst2.text
Sluit2 = Sluit2.text
Open2 = Open2.text

Actueel3 = Actueel3.text
Hoogst3 = Hoogst3.text
Laagst3 = Laagst3.text
Sluit3 = Sluit3.text
Open3 = Open3.text

#vervangt elke komma met een punt zodat ermee gerekent kan worden
a1 = Actueel1.replace(',', '.')
h1 = Hoogst1.replace(',', '.')
l1 = Laagst1.replace(',', '.')
s1 = Sluit1.replace(',', '.')
o1 = Open1.replace(',', '.')

a2 = Actueel2.replace(',', '.')
h2 = Hoogst2.replace(',', '.')
l2 = Laagst2.replace(',', '.')
s2 = Sluit2.replace(',', '.')
o2 = Open2.replace(',', '.')

a3 = Actueel3.replace(',', '.')
h3 = Hoogst3.replace(',', '.')
l3 = Laagst3.replace(',', '.')
s3 = Sluit3.replace(',', '.')
o3 = Open3.replace(',', '.')

#neemt het gemiddelde van elke waarde
Actueel_gem = ((float(a1) + float(a2) + float(a3)) / 3)
Hoogst_gem = ((float(h1) + float(h2) + float(h3)) / 3)
Laagst_gem = ((float(l1) + float(l2) + float(l3)) / 3)
SLuit_gem = ((float(s1) + float(s2) + float(s3)) / 3)
Open_gem = ((float(o1) + float(o2) + float(o3)) / 3)

#variabele voor huidige datum en tijd
datum = dt.datetime.now().strftime('%Y-%m-%d %H:%M')

#maakt kolommen met naam en stppt de data in de juiste kolom
AEX = {'Datum/tijd' : [datum],
        'Actueel' : [round(Actueel_gem, 2)],
        'Hoogst' : [round(Hoogst_gem, 2)],
       'Laagst' : [round(Laagst_gem, 2)],
       'Sluit' : [round(SLuit_gem, 2)],
       'Open' : [round(Open_gem, 2)]}

#pad waar het database bestand zich bevind
path = "/Users/Gebruiker5/Documents/GROEP5/"

#AEX wordt omgezet naar een dataframe
df = pd.DataFrame(AEX)

#connectie wordt gemaakt naar de database
cnx = sqlite3.connect(path + "Database_groep5.sqlite")

#bestand/dataframe wordt toegevoegd aan de meegegeven database
df.to_sql(name="AEX_koers", con=cnx, index=False, if_exists= 'append')

#aanpassingen worden gecommit
cnx.commit()

#connectie wordt gesloten
cnx.close()

print('klaar')
