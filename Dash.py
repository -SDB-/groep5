import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import pandas as pd
import sqlite3
import dash_bootstrap_components as dbc
from datetime import date
import dash_gif_component as gf
import plotly.express as px

conn = sqlite3.connect('Database_groep5.sqlite')

c = conn.cursor()
c1 = conn.cursor()
c2 = conn.cursor()

t = ('RHAT')
today= date.today()
c.execute('SELECT * FROM Ethereum_koers')
c1.execute('SELECT * FROM AEX_koers')
c2.execute('SELECT * FROM Advies ORDER BY Date DESC LIMIT 1')

df = pd.DataFrame(c.fetchall())
df1 = pd.DataFrame(c1.fetchall())
df2 = pd.DataFrame(c2.fetchall())


advies = (df2[3])
MA3 =str((df2[1]))
MA7 =str((df2[2]))


df = df.rename(columns={1 : 'datum/tijd', 2 : 'Price', 3 : 'Hoogst', 4 : 'Laagst', 5 : 'Sluit', 6 : 'Open'})
df = df.drop(columns=0)

df1 = df1.rename(columns={1 : 'datum/tijd', 2 : 'Price', 3 : 'Hoogst', 4 : 'Laagst', 5 : 'Sluit', 6 : 'Open'})
df1 = df1.drop(columns=0)

df['tijd']=pd.to_datetime(df['datum/tijd']).dt.time
df1['tijd']=pd.to_datetime(df1['datum/tijd']).dt.time



# df = px.data.iris()
fig = go.Figure(data=[go.Candlestick(x=df['datum/tijd'].tail(24),
                high=df['Hoogst'].tail(24),
                open=df['Open'].tail(24),
                close=df['Sluit'].tail(24),
                low=df['Laagst'].tail(24),
            )])

fig.update_layout(
    title= ('Actuele koers Ethereum: ' + '€' + str(df['Price'].iloc[-1])+ ' ' + "("+str(df['tijd'].iloc[-1])+ ")"), paper_bgcolor='rgb(211,211,211)')

fig.update_layout(
    title={
        'y':0.9,
        'x':0.5,
        'xanchor': 'center',
        'yanchor': 'top'})






fig1 = go.Figure(data=[go.Candlestick(x=df1['datum/tijd'].tail(24),
                high=df1['Hoogst'].tail(24),
                open=df1['Open'].tail(24),
                close=df1['Sluit'].tail(24),
                low=df1['Laagst'].tail(24))])


fig1.update_layout(
    title= ('Actuele koers AEX: '+ str(df1['Price'].iloc[-1]) + ' ' + "("+str(df['tijd'].iloc[-1])+ ")"), paper_bgcolor='rgb(211,211,211)')

fig1.update_layout(
    title={
        'y':0.9,
        'x':0.5,
        'xanchor': 'center',
        'yanchor': 'top'})




colors = {
    'Background':'D3D3D3'
}

dcc.Graph(figure=fig)
dcc.Graph(figure=fig1)


# fig.show()
external_stylesheets1 = ['Dash.css']



app = dash.Dash(external_stylesheets=external_stylesheets1)


app.layout = html.Div(children=[
    html.Div([

        html.H1('Cryptotrader'),

        gf.GifPlayer(gif='assets\ETH1.gif', still='assets\ETH', autoplay=True),

        html.H1(today)], className='row', style = {'width': '100%', 'display': 'flex', 'align-items': 'center', 'justify-content': 'center', 'backgroundColor':'#D3D3D3'}),



    html.Div([

        dcc.Graph(
            figure=go.Figure(fig)
            ),

        dcc.Graph(
            figure=go.Figure(fig1))], className='row', style = {'width': '100%', 'display': 'flex', 'align-items': 'center', 'justify-content': 'center','backgroundColor':'#D3D3D3'}),


    html.Div([
        html.H1('Advies:' +' '+ advies)
        ], style = {'width': '100%', 'display': 'flex', 'align-items': 'center', 'justify-content': 'center', 'backgroundColor':'#D3D3D3'}),
    html.Div([
        html.H3('Voortschrijdend gemiddelde van ETH over de afgelopen 3 dagen:' + ' ' + ' '+str(df2[1].iloc[-1])),
        ], style = {'width': '100%', 'display': 'flex', 'align-items': 'center', 'justify-content': 'center', 'backgroundColor':'#D3D3D3'}),
    html.Div([
        html.H3('Voortschrijdend gemiddelde van ETH over de afgelopen 7 dagen:'+ ' ' + ' '+str(df2[2].iloc[-1])),
        ], style={'width': '100%', 'display': 'flex', 'align-items': 'center', 'justify-content': 'center','backgroundColor': '#D3D3D3'}),
    html.Div([
        html.H5('Het advies inkopen wordt gegeven wanneer het voortschrijdend gemiddelde van ETH de afgelopen 3 dagen hoger is dan het voortschrijdend gemiddelde van ETH de afgelopen 7 dagen.')
        ], style = {'width': '100%', 'display': 'flex', 'align-items': 'center', 'justify-content': 'center','backgroundColor': '#D3D3D3'})])


if __name__ == '__main__':
    app.run_server(debug=True, host='127.0.0.5')
